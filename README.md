# MC3-Docker
## What is this?
Sets up the following services:

- MongoDB for meta-information
- InfluxDB for time-series data
- Grafana for data-visualization
- Mosquitto as a message broker

## What do the files mean?
- `setup.sh` is not to be run automatically, but just as a guide
- `.env` passwords should be changed
- `ufw_rules.txt` a suggestion for a firewall setup

## How can it do more?
- You can build your NodeJS apps into docker-images and add them to the
  `docker-compose.yml` using [this tutorial](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)
