# run protection
echo "This file should not be run"
exit 1

apt update
apt install docker docker-compose mosquitto #mosquitto only for the util functions
apt upgrade
reboot

# setup config files
mkdir -p /var/mongodb/data

mkdir -p /var/mosquitto/data
mkdir -p /var/mosquitto/log
cp var/mosquitto.conf /var/mosquitto/mosquitto.conf
mosquitto_passwd -c /var/mosquitto/data/passwd mosquitto_user

mkdir -p /var/influxdb/data
mkdir -p /var/influxdb/init
cp var/influxdb.conf /var/influxdb/influxdb.conf

mkdir -p /var/grafana/data
mkdir -p /var/grafana/provisioning
cp var/grafana.ini /var/grafana/grafana.ini
chown 472:472 /var/grafana/data

# start docker
docker-compose up -d

# setup mongodb
docker exec -it mongo-meta-inf bash
mongo admin -u superuser -p secret_mongosuperpass
use meta-inf
db.createUser({user: "mongo-user", pwd: "secret_mongopass", roles: [{role: "readWrite", db:"meta-inf"}]})
